# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

The Twitter Streaming API provides real�time access to public tweets. In this assignment you will build an iOS application that connects to the Streaming API and processes incoming tweets to compute various statistics. This is your chance to demonstrate to us that you�re capable of building a modern, functioning application using solid software development skills. Please submit your best possible work that you would be proud to show to others.
The sample endpoint provides a random sample of approximately 1% of the full tweet stream. Your app should consume this sample stream and keep track of the following:

* Total number of tweets received
* Average tweets per hour/minute/second
* Top emojis in tweets
* Percent of tweets that contains emojis
* Top hashtags
* Percent of tweets that contain a url
* Percent of tweets that contain a photo url (pic.twitter.com or instagram)
* Top domains of urls in tweets

### How do I get set up? ###

* In order to successfully run this app, you must run on an actual device, and you must be logged into Twitter on the system: Settings > Twitter.