//
//  MockTweetProvider.swift
//  TwitterStreaming
//
//  Created by Brad Heath on 4/23/17.
//  Copyright © 2017 Brad Heath. All rights reserved.
//

import Foundation

class MockTweetProvider {

    var tweets = [Tweet]()

    init() {
        populateMockTweets()
    }

    func populateMockTweets() {
        let tweet1 = Tweet()
        tweet1.containsEmojis = true
        tweet1.containsPhotoUrl = true
        tweet1.containsUrl = true
        tweet1.emojis = ["☺️", "😊", "💯"]
        tweet1.domains = ["twitter.com", "google.com", "instagram.com"]
        tweet1.hashTags = ["hello_world", "hi_mom", "go_sports_team"]
        tweets.append(tweet1)

        let tweet2 = Tweet()
        tweet2.containsEmojis = true
        tweet2.containsPhotoUrl = true
        tweet2.containsUrl = true
        tweet2.emojis = ["💩", "😊", "♻️"]
        tweet2.domains = ["twitter.com", "google.com", "infowars.com"]
        tweet2.hashTags = ["hello_world", "hello", "hey"]
        tweets.append(tweet2)

        let tweet3 = Tweet()
        tweet3.containsEmojis = true
        tweet3.containsPhotoUrl = true
        tweet3.containsUrl = true
        tweet3.emojis = ["☺️", "😎", "😤"]
        tweet3.domains = ["twitter.com", "bing.com", "pbs.twimg.com"]
        tweet3.hashTags = ["hello_world", "hi_mom", "freepeltier"]
        tweets.append(tweet3)

        let tweet4 = Tweet()
        tweet4.containsEmojis = true
        tweet4.containsPhotoUrl = true
        tweet4.containsUrl = true
        tweet4.emojis = ["☺️", "😊", "🌁"]
        tweet4.domains = ["twitter.com", "bing.com", "reddit.com"]
        tweet4.hashTags = ["hello_world", "asdfa", "go_sports_team"]
        tweets.append(tweet4)

        let tweet5 = Tweet()
        tweet5.containsEmojis = true
        tweet5.containsPhotoUrl = true
        tweet5.containsUrl = true
        tweet5.emojis = ["☺️", "😊", "🤠"]
        tweet5.domains = ["twitter.com", "google.com", "stackoverflow.com"]
        tweet5.hashTags = ["hello_world", "hi_masdfaom", "go_sports_team"]
        tweets.append(tweet5)

        let tweet6 = Tweet()
        tweet6.containsEmojis = true
        tweet6.containsPhotoUrl = true
        tweet6.containsUrl = true
        tweet6.emojis = ["☺️", "😊", "💯"]
        tweet6.domains = ["twitter.com", "google.com", "apple.com"]
        tweet6.hashTags = ["hello_world", "asdf", "falala"]
        tweets.append(tweet6)

        let tweet7 = Tweet()
        tweet7.containsEmojis = true
        tweet7.containsPhotoUrl = true
        tweet7.containsUrl = true
        tweet7.emojis = ["☺️", "😊", "🤠"]
        tweet7.domains = ["twitter.com", "google.com", "cnn.com"]
        tweet7.hashTags = ["hello_world", "asdf", "ewr"]
        tweets.append(tweet7)

        let tweet8 = Tweet()
        tweet8.containsEmojis = true
        tweet8.containsPhotoUrl = true
        tweet8.containsUrl = true
        tweet8.emojis = ["☺️", "😊", "💯"]
        tweet8.domains = ["twitter.com", "google.com", "foxnews.com"]
        tweet8.hashTags = ["hello_world", "hi_mom", "ewr"]
        tweets.append(tweet8)

        let tweet9 = Tweet()
        tweet9.containsEmojis = true
        tweet9.containsPhotoUrl = true
        tweet9.containsUrl = true
        tweet9.emojis = ["☺️", "😊", "😤"]
        tweet9.domains = ["twitter.com", "weather.com", "npr.org"]
        tweet9.hashTags = ["hello_world", "adga", "fsdfa"]
        tweets.append(tweet9)

        let tweet10 = Tweet()
        tweet10.containsEmojis = true
        tweet10.containsPhotoUrl = true
        tweet10.containsUrl = true
        tweet10.emojis = ["☺️", "😊", "💯"]
        tweet10.domains = ["twitter.com", "google.com", "reddit.com"]
        tweet10.hashTags = ["hello_world", "asdf", "asdf"]
        tweets.append(tweet10)
        
    }
}
