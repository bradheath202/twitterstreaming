//
//  TwitterStreamingDataManagerTests.swift
//  TwitterStreaming
//
//  Created by Brad Heath on 4/22/17.
//  Copyright © 2017 Brad Heath. All rights reserved.
//

import XCTest
@testable import TwitterStreaming

class TwitterStreamingDataManagerTests: XCTestCase {
    let 💯Percent = "100.00%"

    func testTweetRate() {
        let dataManager = TwitterStreamingDataManager()
        let tweets = MockTweetProvider().tweets
        dataManager.setTweets(tweets as! NSMutableArray)
        dataManager.updateStatistics()

        guard let twitterData = dataManager.twitterData
            else {
                XCTFail("twitterData is nil")
                return
        }

        let topHashtags = twitterData[kTopHashtags]!
        let topDomains = twitterData[kTopDomains]!
        let topEmoji = twitterData[kTopEmojis]!
        let tweetPercentagesUrl = twitterData[kTweetPercentagesUrl] as! String
        let tweetPercentagesPhotoUrl = twitterData[kTweetPercentagesPhotoUrl] as! String
        let tweetPercentagesEmoji = twitterData[kTweetPercentagesEmojis] as! String

        XCTAssert((topHashtags as AnyObject).count > 0, "topHashtags is empty")
        XCTAssert((topDomains as AnyObject).count > 0, "topDomains is empty")
        XCTAssert((topEmoji as AnyObject).count > 0, "topEmoji is empty")
        XCTAssert(tweetPercentagesUrl == 💯Percent, "tweetPercentagesUrl is incorrect")
        XCTAssert(tweetPercentagesPhotoUrl == 💯Percent, "tweetPercentagesPhotoUrl is incorrect")
        XCTAssert(tweetPercentagesEmoji == 💯Percent, "tweetPercentagesEmoji is incorrect")

    }
}
