//
//  TwitterStreamingManager.m
//  TwitterStreaming
//
//  Created by Brad Heath on 4/10/17.
//  Copyright © 2017 Brad Heath. All rights reserved.
//

#import "TwitterStreamingDataManager.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "StreamProcessingOperation.h"
#import "Tweet.h"

typedef NS_ENUM(NSInteger, TwitterDataType) {
    TwitterDataTypeUrl = 0,
    TwitterDataTypeUrlPhoto,
    TwitterDataTypeEmoji,
    TwitterDataTypeHashtags
};

static NSString *kStreamingUrl = @"https://stream.twitter.com/1.1/statuses/sample.json";

NSString* const kTweetTotal = @"tweetTotal";
NSString* const kTweetRate = @"tweetRate";
NSString* const kTweetPercentagesUrl = @"tweetPercentagesUrl";
NSString* const kTweetPercentagesPhotoUrl = @"tweetPercentagesPhotoUrl";
NSString* const kTweetPercentagesEmojis = @"tweetPercentagesEmoji";
NSString* const kTopHashtags = @"topHashtags";
NSString* const kTopDomains = @"topDomains";
NSString* const kTopEmojis = @"topEmoji";
NSString* const kHashtags = @"hashtags";
NSString* const kDomains = @"domains";
NSString* const kEmojis = @"emojis";

NSString* const kError = @"error";
NSString* const kAccountIssue = @"There was a problem logging in to Twitter. Please check your settings: Settings > Twitter, and ensure you are logged in and that you have given this app permission to access your account, then restart the app.";

NSString* const kStreamProcessingCompleted = @"StreamProcessingOperationCompleted";
NSString* const kTwitterDataUpdated = @"TwitterDataUpdated";
NSString* const kAuthenticationError = @"AuthenticationError";

static NSInteger kTimeIntervalSeconds = 1.0;
static NSInteger kSecondsInMinute = 60;
static NSInteger kSecondsInHour = 3600;

@interface TwitterStreamingDataManager()

@property (nonatomic, strong) NSMutableData *incomingTweetBuffer;
@property (nonatomic, assign) BOOL processingStarted;
@property (nonatomic, strong) NSTimer *processingTimer;

//@property (nonatomic, assign) NSUInteger runningTotal;
@property (nonatomic, assign) NSUInteger tweetsPerSecond;

@property (nonatomic, strong) NSMutableArray *tweets;

@property (nonatomic, strong) NSOperationQueue *processingQueue;
@property (nonatomic, strong) StreamProcessingOperation *processingOperation;

@end

@implementation TwitterStreamingDataManager

- (instancetype)init {
    if (self = [super init]) {
        //self.runningTotal = 0;
        self.incomingTweetBuffer = [[NSMutableData alloc] init];
        self.tweets = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)startStreamingService {
    self.processingQueue = [[NSOperationQueue alloc] init];
    [self startOrResumeStreamProcessing];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleProcessedTweets:) name:kStreamProcessingCompleted object:nil];

    if ([self userHasSystemTwitterAccount]) {
        ACAccountStore *store = [[ACAccountStore alloc] init];
        ACAccountType *twitterAccountType = [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        [store requestAccessToAccountsWithType:twitterAccountType
                                       options:nil
                                    completion:^(BOOL granted, NSError *error) {
                                        if (error) {
                                            [[NSNotificationCenter defaultCenter] postNotificationName:kAuthenticationError object:nil userInfo:@{ kError : error.localizedDescription }];
                                        } else {
                                            if (!granted) {
                                                [[NSNotificationCenter defaultCenter] postNotificationName:kAuthenticationError object:nil userInfo:@{ kError : kAccountIssue}];
                                            } else {
                                                NSArray *twitterAccounts = [store accountsWithAccountType:twitterAccountType];
                                                if ([twitterAccounts count] > 0) {
                                                    ACAccount *account = [twitterAccounts lastObject];

                                                    NSURL *url = [NSURL URLWithString:kStreamingUrl];
                                                    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                                                            requestMethod:SLRequestMethodPOST
                                                                                                      URL:url
                                                                                               parameters:nil];

                                                    [request setAccount:account];

                                                    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
                                                    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
                                                    NSURLSessionDataTask *task = [session dataTaskWithRequest:request.preparedURLRequest];
                                                    [task resume];
                                                }
                                            }
                                        }
                                    }];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kAuthenticationError object:nil userInfo:@{ kError : kAccountIssue }];
    }
}

#pragma mark - StreamProcessingOperationDelegate

- (void)streamProcessingCompleted:(NSDictionary *)processedTweets {
    [self handleProcessedTweets:processedTweets];
}

#pragma mark - Notification Handlers

- (void)handleProcessedTweets:(NSDictionary *)processedTweets {
    NSArray *tweets = [processedTweets objectForKey:@"processedTweets"];

    self.tweetsPerSecond = processedTweets.count;
    //self.runningTotal += processedTweets.count;

    for (NSDictionary *aTweet in tweets) {
        if (![aTweet objectForKey:@"delete"]) {
            Tweet *tweet = [[Tweet alloc] initWithDictionary:aTweet];
            [self.tweets addObject:tweet];
        }
    }

    [self updateStatistics];
    [self startOrResumeStreamProcessing];
}

#pragma mark - Helpers/Utility

- (BOOL)userHasSystemTwitterAccount {
    return [SLComposeViewController
     isAvailableForServiceType:SLServiceTypeTwitter];
}

- (void)startOrResumeStreamProcessing {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.processingTimer = [NSTimer scheduledTimerWithTimeInterval:kTimeIntervalSeconds target:self selector:@selector(processStream) userInfo:nil repeats:YES];
        self.processingStarted = NO;

        [[NSRunLoop currentRunLoop] addTimer:self.processingTimer
                                     forMode:NSRunLoopCommonModes];
    });

}

- (void)pauseStreamProcessing {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.processingTimer invalidate];
        self.processingStarted = YES;
    });

}

- (void)processStream {
    if ([self.incomingTweetBuffer length] > 0) {

        self.processingQueue.maxConcurrentOperationCount = 1;
        self.processingQueue.name = @"Stream Processing Queue";

        self.processingOperation = [[StreamProcessingOperation alloc] initWithData:[self.incomingTweetBuffer copy]];
        self.processingOperation.delegate = self;
        [self.processingQueue addOperation:self.processingOperation];

        [self.incomingTweetBuffer setData:[NSData data]];
        [self pauseStreamProcessing];
    }
}

- (void)cancelStreamProcessing {
    [self pauseStreamProcessing];
    [self.processingQueue cancelAllOperations];
    self.processingQueue = nil;
    self.processingOperation = nil;
    //self.runningTotal = 0;
}

- (void)updateStatistics {

    NSString *tweetTotal = [NSString stringWithFormat:@"%li", self.tweets.count];
    NSArray *tweetRate = [self tweetRate];
    NSString *tweetPercentageUrl = [self percentOfTweetsContainingDataType:TwitterDataTypeUrl];
    NSString *tweetPercentagePhotoUrl = [self percentOfTweetsContainingDataType:TwitterDataTypeUrlPhoto];
    NSString *tweetPercentageEmojis = [self percentOfTweetsContainingDataType:TwitterDataTypeEmoji];
    NSArray *topHashtags = [self topFiveOfDataType:TwitterDataTypeHashtags] ? [self topFiveOfDataType:TwitterDataTypeHashtags] : @[];
    NSArray *topDomains = [self topFiveOfDataType:TwitterDataTypeUrl] ? [self topFiveOfDataType:TwitterDataTypeUrl] : @[];
    NSArray *topEmojis = [self topFiveOfDataType:TwitterDataTypeEmoji] ? [self topFiveOfDataType:TwitterDataTypeEmoji] : @[];

    self.twitterData = @{
                         kTweetTotal : tweetTotal,
                         kTweetRate : tweetRate,
                         kTweetPercentagesUrl : tweetPercentageUrl,
                         kTweetPercentagesPhotoUrl : tweetPercentagePhotoUrl,
                         kTweetPercentagesEmojis : tweetPercentageEmojis,
                         kTopHashtags : topHashtags,
                         kTopDomains : topDomains,
                         kTopEmojis : topEmojis
                         };

//    [[NSNotificationCenter defaultCenter] postNotificationName:kTwitterDataUpdated object:nil userInfo:[self.twitterData copy]];
    [self.delegate twitterDataProcessingCompleted:[self.twitterData copy]];
}

- (NSArray *)tweetRate {

    NSString *tweetsPerSecond = [NSString stringWithFormat:@"%lu", (unsigned long)self.tweetsPerSecond];
    NSString *tweetsPerMinute = [NSString stringWithFormat:@"%lu", (unsigned long)self.tweetsPerSecond * kSecondsInMinute];
    NSString *tweetsPerHour = [NSString stringWithFormat:@"%lu", (unsigned long)self.tweetsPerSecond * kSecondsInHour];

    NSLog(@"Tweets/s: %@", tweetsPerSecond);
    NSLog(@"Tweets/minute: %@", tweetsPerMinute);
    NSLog(@"Tweets/hour: %@", tweetsPerHour);
    NSLog(@"Total Tweets received: %ld", self.tweets.count);

    NSArray *tweetRates = @[tweetsPerSecond, tweetsPerMinute, tweetsPerHour];

    return tweetRates;
}

- (NSString *)percentOfTweetsContainingDataType:(TwitterDataType)dataType {
    float percentOfTweetsContainingData = 0.0f;
    float dataCount = 0.0f;

    for (Tweet *tweet in self.tweets) {
        switch (dataType) {
            case TwitterDataTypeUrl:
                if (tweet.containsUrl) {
                    dataCount++;
                }
                break;
            case TwitterDataTypeUrlPhoto:
                if (tweet.containsPhotoUrl) {
                    dataCount++;
                }
                break;
            case TwitterDataTypeEmoji:
                if (tweet.containsEmojis) {
                    dataCount++;
                }
                break;
            default:
                break;
        }

    }

    percentOfTweetsContainingData = dataCount/self.tweets.count;
    NSString *formattedPercentOfTweetsContainingData = [NSString stringWithFormat:@"%.2f%%", percentOfTweetsContainingData * 100];

    return formattedPercentOfTweetsContainingData;
}

- (NSArray *)topFiveOfDataType:(TwitterDataType)dataType {
    NSMutableArray *dataTypeArray = [[NSMutableArray alloc] init];
    NSString *dataKey;
    for (Tweet *tweet in self.tweets) {
        switch (dataType) {
            case TwitterDataTypeHashtags: {
                NSArray *tweetHashtags = tweet.hashTags;
                [dataTypeArray addObjectsFromArray:tweetHashtags];
                dataKey = kHashtags;
            }
                break;
            case TwitterDataTypeUrl: {
                NSArray *tweetDomains = tweet.domains;
                [dataTypeArray addObjectsFromArray:tweetDomains];
                dataKey = kDomains;
            }
                break;
            case TwitterDataTypeEmoji: {
                NSArray *tweetEmojis = tweet.emojis;
                [dataTypeArray addObjectsFromArray:tweetEmojis];
                dataKey = kEmojis;
            }
                break;

            default:
                break;
        }

    }

    NSCountedSet *countedSet = [[NSCountedSet alloc] initWithArray:dataTypeArray];

    NSMutableArray *sortedArray = [[NSMutableArray alloc] init];
    for (NSString *datum in countedSet) {

        [sortedArray addObject:@{dataKey: [NSString stringWithFormat:@"%@ (%lu)", datum, (unsigned long)[countedSet countForObject:datum]],
                                 @"count": @([countedSet countForObject:datum])}];
    }

    sortedArray = [[sortedArray sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"count" ascending:NO]]] mutableCopy];

    NSArray *topFiveOfDataType;

    if (sortedArray.count) {
        if (sortedArray.count >= 5) {
            topFiveOfDataType = [sortedArray subarrayWithRange:NSMakeRange(0, 5)];
        } else {
            topFiveOfDataType = [sortedArray subarrayWithRange:NSMakeRange(0, sortedArray.count)];
        }
    }

    return  topFiveOfDataType;
}

#pragma mark - NSURLSessionDataDelegate

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data {
    if (self.processingStarted == NO) {
        [self.incomingTweetBuffer appendData:data];
    }
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
