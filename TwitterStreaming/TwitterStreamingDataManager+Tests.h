//
//  TwitterStreamingDataManager+Tests.h
//  TwitterStreaming
//
//  Created by Brad Heath on 4/23/17.
//  Copyright © 2017 Brad Heath. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TwitterStreamingDataManager.h"

@interface TwitterStreamingDataManager(Tests)

- (void)setTweets:(NSMutableArray *)tweets;
- (void)updateStatistics;

@end
