//
//  TwitterStreamStatsViewController.m
//  TwitterStreaming
//
//  Created by Brad Heath on 4/10/17.
//  Copyright © 2017 Brad Heath. All rights reserved.
//

#import "TwitterStreamStatsTableViewController.h"
#import "TwitterStreamingDataManager.h"

@interface TwitterStreamStatsTableViewController ()

@property (nonatomic, weak) IBOutlet UILabel *totalTweets;
@property (nonatomic, weak) IBOutlet UILabel *tweetsSecond;
@property (nonatomic, weak) IBOutlet UILabel *tweetsMinute;
@property (nonatomic, weak) IBOutlet UILabel *tweetsHour;
@property (nonatomic, weak) IBOutlet UILabel *tweetsUrl;
@property (nonatomic, weak) IBOutlet UILabel *tweetsPhotoUrl;
@property (nonatomic, weak) IBOutlet UILabel *tweetsEmoji;

@property (nonatomic, weak) IBOutlet UILabel *topDomains;
@property (nonatomic, weak) IBOutlet UILabel *topHashtags;
@property (nonatomic, weak) IBOutlet UILabel *topEmojis;

@property (nonatomic, weak) UIBarButtonItem *reset;

@property (nonatomic, strong) TwitterStreamingDataManager *dataManager;

@end

@implementation TwitterStreamStatsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerForNotifications];
    [self initializeDataManager];
}

- (void)initializeDataManager {
    // set up the data manager
    if (self.dataManager) {
        [self.dataManager cancelStreamProcessing];
    }
    self.dataManager = [[TwitterStreamingDataManager alloc] init];
    self.dataManager.delegate = self;
    [self.dataManager startStreamingService];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)registerForNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authenticationError:) name:kAuthenticationError object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - TwitterStreamingDataManagerDelegate

- (void)twitterDataProcessingCompleted:(NSDictionary *)twitterData {
    [self updateTwitterDisplayData:twitterData];
}

#pragma mark - notification handlers

- (void)updateTwitterDisplayData:(NSDictionary *)twitterData {
    dispatch_async(dispatch_get_main_queue(), ^{

        NSDictionary *data = twitterData;

        self.totalTweets.text = [data objectForKey:kTweetTotal];
        self.tweetsSecond.text = [data objectForKey:kTweetRate][0];
        self.tweetsMinute.text = [data objectForKey:kTweetRate][1];
        self.tweetsHour.text = [data objectForKey:kTweetRate][2];
        self.tweetsUrl.text = [data objectForKey:kTweetPercentagesUrl];
        self.tweetsPhotoUrl.text = [data objectForKey:kTweetPercentagesPhotoUrl];
        self.tweetsEmoji.text = [data objectForKey:kTweetPercentagesEmojis];

        NSArray *domains = [data objectForKey:kTopDomains];
        NSMutableArray *displayDomains = [[NSMutableArray alloc] init];
        for (NSDictionary *dict in domains) {
            [displayDomains addObject:[dict objectForKey:kDomains]];
        }
        self.topDomains.text = [displayDomains componentsJoinedByString:@", "];

        NSArray *hashtags = [data objectForKey:kTopHashtags];
        NSMutableArray *displayHashtags = [[NSMutableArray alloc] init];
        for (NSDictionary *dict in hashtags) {
            [displayHashtags addObject:[dict objectForKey:kHashtags]];
        }
        self.topHashtags.text = [displayHashtags componentsJoinedByString:@", "];

        NSArray *emojis = [data objectForKey:kTopEmojis];
        NSMutableArray *displayEmojis = [[NSMutableArray alloc] init];
        for (NSDictionary *dict in emojis) {
            [displayEmojis addObject:[dict objectForKey:kEmojis]];
        }
        self.topEmojis.text = [displayEmojis componentsJoinedByString:@", "];
    });
}

- (void)authenticationError:(NSNotification *)notification {
    NSString *message = [notification.userInfo objectForKey:kError];

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message: message preferredStyle:UIAlertControllerStyleAlert];

    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([message isEqualToString:kAccountIssue] || [message isEqualToString:kAccountIssue]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
        }
    }]];

    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Actions

- (IBAction)resetStreaming:(id)sender {
    [self resetDisplay];
    [self initializeDataManager];
}

#pragma mark - Helpers

- (void)resetDisplay {
    self.totalTweets.text = nil;
    self.tweetsSecond.text = nil;
    self.tweetsMinute.text = nil;
    self.tweetsHour.text = nil;
    self.tweetsUrl.text = nil;
    self.tweetsPhotoUrl.text = nil;
    self.tweetsEmoji.text = nil;
}


@end
