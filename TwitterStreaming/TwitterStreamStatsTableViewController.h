//
//  TwitterStreamStatsViewController.h
//  TwitterStreaming
//
//  Created by Brad Heath on 4/10/17.
//  Copyright © 2017 Brad Heath. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TwitterStreamingDataManager.h"

@interface TwitterStreamStatsTableViewController : UITableViewController <TwitterStreamingDataManagerDelegate>

@end
