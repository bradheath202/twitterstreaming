//
//  StreamProcessingOperation.h
//  TwitterStreaming
//
//  Created by Brad Heath on 4/11/17.
//  Copyright © 2017 Brad Heath. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol StreamProcessingOperationDelegate

- (void)streamProcessingCompleted:(NSDictionary *)processedTweets;

@end

@interface StreamProcessingOperation : NSOperation

@property (nonatomic, strong) NSArray *tweetsArray;
@property (nonatomic, weak) id<StreamProcessingOperationDelegate> delegate;

-(instancetype)initWithData:(NSData *)data;

@end
