//
//  AppDelegate.h
//  TwitterStreaming
//
//  Created by Brad Heath on 4/10/17.
//  Copyright © 2017 Brad Heath. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

