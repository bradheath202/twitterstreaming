//
//  Tweet.h
//  TwitterStreaming
//
//  Created by Brad Heath on 4/11/17.
//  Copyright © 2017 Brad Heath. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tweet : NSObject

@property (nonatomic, assign) BOOL containsEmojis;
@property (nonatomic, assign) BOOL containsUrl;
@property (nonatomic, assign) BOOL containsPhotoUrl;

@property (nonatomic, strong) NSMutableArray *hashTags;
@property (nonatomic, strong) NSMutableArray *domains;
@property (nonatomic, strong) NSArray *emojis;

@property (nonatomic, strong) NSString *tweetText;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
