//
//  StreamProcessingOperation.m
//  TwitterStreaming
//
//  Created by Brad Heath on 4/11/17.
//  Copyright © 2017 Brad Heath. All rights reserved.
//

#import "StreamProcessingOperation.h"
#import "TwitterStreamingDataManager.h"

@interface StreamProcessingOperation()

@property (nonatomic, strong) NSData *data;

@end

@implementation StreamProcessingOperation

- (instancetype)initWithData:(NSData *)data {
    if (self = [super init]) {
        self.data = data;
    }
    return self;
}

- (void)main {
    [self processStreamData:self.data];
}

- (void)processStreamData:(NSData *)data {
    NSError *error = nil;

    NSString *tempString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    NSArray *streamArray = [tempString componentsSeparatedByString:@"\r\n"];

    NSMutableArray *tweetsArray = [NSMutableArray array];
    for (NSString *streamObject in streamArray) {
        NSData *streamObjectData = [streamObject dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *tweet = [NSJSONSerialization JSONObjectWithData:streamObjectData options:0 error:&error];

        if (error) {
            NSLog(@"Error parsing Tweet data: %@", error.localizedDescription);
        } else {
            [tweetsArray addObject:tweet];
        }
    }

    [self.delegate streamProcessingCompleted:@{@"processedTweets" : tweetsArray}];
//    [[NSNotificationCenter defaultCenter] postNotificationName:kStreamProcessingCompleted
//                                                        object:nil userInfo:@{@"processedTweets": tweetsArray}];
}

@end
