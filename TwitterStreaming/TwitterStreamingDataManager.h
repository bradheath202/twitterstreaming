//
//  TwitterStreamingManager.h
//  TwitterStreaming
//
//  Created by Brad Heath on 4/10/17.
//  Copyright © 2017 Brad Heath. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StreamProcessingOperation.h"

extern NSString* const kTweetTotal;
extern NSString* const kTweetRate;
extern NSString* const kTweetPercentagesUrl;
extern NSString* const kTweetPercentagesPhotoUrl;
extern NSString* const kTweetPercentagesEmojis;
extern NSString* const kTopHashtags;
extern NSString* const kTopDomains;
extern NSString* const kTopEmojis;
extern NSString* const kHashtags;
extern NSString* const kDomains;
extern NSString* const kEmojis;

extern NSString* const kError;
extern NSString* const kAccountIssue;

extern NSString* const kStreamProcessingCompleted;
extern NSString* const kTwitterDataUpdated;
extern NSString* const kAuthenticationError;

@protocol TwitterStreamingDataManagerDelegate

- (void)twitterDataProcessingCompleted:(NSDictionary *)twitterData;

@end

@interface TwitterStreamingDataManager : NSObject <NSURLSessionDataDelegate, StreamProcessingOperationDelegate>

@property (nonatomic, weak) id<TwitterStreamingDataManagerDelegate> delegate;
@property (nonatomic, strong) NSDictionary *twitterData;

- (void)startStreamingService;
- (void)cancelStreamProcessing;


@end
