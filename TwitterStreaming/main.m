//
//  main.m
//  TwitterStreaming
//
//  Created by Brad Heath on 4/10/17.
//  Copyright © 2017 Brad Heath. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
