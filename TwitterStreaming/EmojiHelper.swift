//
//  EmojiHelper.swift
//  TwitterStreaming
//
// found at http://stackoverflow.com/questions/39121989/extract-emoji-unicode-from-nsstring

import Foundation

@objc public class EmojiHelper: NSObject {

    func emojisIncludingCombined(_ str: String) -> [String]  {

        let emojiPattern1 = "[\\p{Emoji_Presentation}\\u26F1]" //Code Points with default emoji representation
        let emojiPattern2 = "\\p{Emoji}\\uFE0F" //Characters with emoji variation selector
        let emojiPattern3 = "\\p{Emoji_Modifier_Base}\\p{Emoji_Modifier}" //Characters with emoji modifier
        let emojiPattern4 = "[\\U0001F1E6-\\U0001F1FF][\\U0001F1E6-\\U0001F1FF]" //2-letter flags
        let pattern = "\(emojiPattern4)|\(emojiPattern3)|\(emojiPattern2)|\(emojiPattern1)"
        let combinedPattern = "(?:\(pattern))(?:\\u200D(?:\(pattern)))*"
        
        let regex = try! NSRegularExpression(pattern: combinedPattern, options: [])
        let matches = regex.matches(in: str, options: [], range: NSRange(0..<str.utf16.count))

        return matches.map{(str as NSString).substring(with: $0.range)}
    }
}
