//
//  Tweet.m
//  TwitterStreaming
//
//  Created by Brad Heath on 4/11/17.
//  Copyright © 2017 Brad Heath. All rights reserved.
//

#import "Tweet.h"
#import "TwitterStreaming-Swift.h"

static NSString *kEntities = @"entities";
static NSString *kHashTags = @"hashtags";
static NSString *kText = @"text";
static NSString *kUrls = @"urls";
static NSString *kExpandedUrl = @"expanded_url";
static NSString *kTwitterImageSource = @"pbs.twimg.com";
static NSString *kInstagramImageSource = @"instagram";


@implementation Tweet

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        self.containsUrl = NO;
        self.containsPhotoUrl = NO;
        self.containsEmojis = NO;

        [self configureWithDictionary:dictionary];
    }
    return self;
}

- (void)configureWithDictionary:(NSDictionary *)dictionary {
    NSDictionary *entities = [dictionary objectForKey:kEntities];
    NSArray *hashtags = [entities objectForKey:kHashTags];

    self.tweetText = [dictionary objectForKey:kText];

    EmojiHelper *emojiHelper = [[EmojiHelper alloc] init];
    self.emojis = [emojiHelper emojisIncludingCombined:self.tweetText];
    if (self.emojis && self.emojis.count) {
        self.containsEmojis = YES;
    }

    self.domains = [[NSMutableArray alloc] init];
    self.hashTags = [[NSMutableArray alloc] init];
    for (NSDictionary *innerHashtag in hashtags) {
        [self.hashTags addObject:[innerHashtag objectForKey:kText]];
    }

    NSArray *urls = [entities objectForKey:kUrls];

    for (NSDictionary *urlDict in urls) {
        NSString *expandedUrl = [urlDict objectForKey:kExpandedUrl];

        if (expandedUrl && ![expandedUrl isEqual:[NSNull null]]) {
            self.containsUrl = YES;
            if ([expandedUrl containsString:kTwitterImageSource] || [expandedUrl containsString:kInstagramImageSource]) {
                self.containsPhotoUrl = YES;
            }

            NSURL *url = [NSURL URLWithString:expandedUrl];
            NSString *domain = [url host];
            // Here is a likely culprit of the sporadic crash.
            // urls in some cases cannot be parsed. 
            // putting check here to prevent crash.
            if (domain) {
                [self.domains addObject:domain];
            } else {
                NSLog(@"Domain unable to be parsed.");
            }

        }
    }
}

@end
